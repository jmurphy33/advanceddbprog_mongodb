﻿
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MongoDB
{
    class Database
    {

        IMongoDatabase db;

        public Database()
        {
            var client = new MongoClient();
            db = client.GetDatabase("games");
        }


        // Create

        public void Create_Document(string searchCol, string value)
        {
            var col = db.GetCollection<BsonDocument>(searchCol);

            var document = new BsonDocument();
            document = BsonDocument.Parse(value);
            col.InsertOneAsync(document);
        }

        public void Create_ManyDocuments(string searchCol, IEnumerable<BsonDocument> doc)
        {

            var col = db.GetCollection<BsonDocument>(searchCol);
            col.InsertManyAsync(doc);
        }

        // Read

        public async Task<IAsyncCursor<BsonDocument>> Read_Document(string searchCol, string field, string value)
        {
            var col = db.GetCollection<BsonDocument>(searchCol);
            var filter = Builders<BsonDocument>.Filter.Eq(field, value);
            var searchResult = await col.FindAsync(filter);
            Task<IAsyncCursor<BsonDocument>> x = Task.FromResult(await col.FindAsync(filter));
            return x.Result;
        }

        public async Task<IAsyncCursor<BsonDocument>> Read_AllDocuments(string searchCol)
        {
            var col = db.GetCollection<BsonDocument>(searchCol);
            var filter = new BsonDocument();

            Task<IAsyncCursor<BsonDocument>> x = Task.FromResult(await col.FindAsync(filter));

            return x.Result;
        }

        // Not needed Singular read document works the same
        public async Task<IAsyncCursor<BsonDocument>> Read_ManyDocuments(string searchCol, string searchField, string searchValue)
        {
            var col = db.GetCollection<BsonDocument>(searchCol);
            var filter = Builders<BsonDocument>.Filter.Eq(searchField, searchValue);         
            Task<IAsyncCursor<BsonDocument>> x = Task.FromResult(await col.FindAsync(filter));
            return x.Result;
        }


        // Update
        public async void Update_Document(string searchCol, string field, string value, string newField, string newValue)
        {
            var col = db.GetCollection<BsonDocument>(searchCol);
            var filter = Builders<BsonDocument>.Filter.Eq(field, value);
            var update = Builders<BsonDocument>.Update.Set(newField, newValue);
            var result = await col.UpdateOneAsync(filter, update);
            var cursor = await col.FindAsync(filter);
        }

        public async void Update_AllDocuments(string searchCol, string newField, string newValue)
        {
            var col = db.GetCollection<BsonDocument>(searchCol);
            var filter = new BsonDocument();
            if (newValue.StartsWith("{")){
                BsonDocument val = BsonDocument.Parse(newValue);
                var update = Builders<BsonDocument>.Update.Set(newField, val);
                var result = await col.UpdateManyAsync(filter, update);
            }

            else
            {
                var update = Builders<BsonDocument>.Update.Set(newField, newValue);
                var result = await col.UpdateManyAsync(filter, update);
            }
            
        }

        public async void Update_ManyDocuments(string searchCol, string searchField, string searchValue, string newField, string newValue)
        {
            FilterDefinition<BsonDocument> filter;
            var col = db.GetCollection<BsonDocument>(searchCol);
            if (searchValue.StartsWith("{"))
            {
                BsonDocument val = BsonDocument.Parse(searchValue);
                filter = Builders<BsonDocument>.Filter.Eq(searchField, val);
            }

            else
            {
                filter = Builders<BsonDocument>.Filter.Eq(searchField, searchValue);
            }

            if (newValue.StartsWith("{"))
            {
                BsonDocument val = BsonDocument.Parse(newValue);
                var update = Builders<BsonDocument>.Update.Set(newField, val);
                var result = await col.UpdateManyAsync(filter, update);
            }

            else
            {
                var update = Builders<BsonDocument>.Update.Set(newField, newValue);
                var result = await col.UpdateManyAsync(filter, update);
            }
        }

        // Delete

        public void Delete_Document(string searchCol, string name)
        {
            var col = db.GetCollection<BsonDocument>(searchCol);
            col.DeleteOneAsync(name);
        }

        public async void Delete_ManyDocuments(string searchCol, string field, string value)
        {
            FilterDefinition<BsonDocument> filter;
            var col = db.GetCollection<BsonDocument>(searchCol);
            if (value.StartsWith("{"))
            {
                BsonDocument val = BsonDocument.Parse(value);
                filter = Builders<BsonDocument>.Filter.Eq(field, val);
            }

            else
            {
                filter = Builders<BsonDocument>.Filter.Eq(field, value);
            }


            await col.DeleteManyAsync(filter);
        }

        public async void Delete_AllDocuments(string searchCol)
        {

            var col = db.GetCollection<BsonDocument>(searchCol);
            var filter = new BsonDocument();
            await col.DeleteManyAsync(filter);

        }


        // Map Reduce

        public async Task<IAsyncCursor<BsonDocument>> MapReduce(string searchCol)
        {

            var col = db.GetCollection<BsonDocument>(searchCol);

            string map = @"function() { 
                                 var game = this;
                                 var gameList = this.GameList;
                                if(gameList != null){                                    
                                         gameList.forEach(function(GameList){  
                                                      emit(game.name, 1);
                                        });
                                }
                                } ";

            string reducer = @"  function(key, values) { return values.length;  } ";

            BsonJavaScript mapJS = new BsonJavaScript(map);
            BsonJavaScript reducerJS = new BsonJavaScript(reducer);


            var find = new MapReduceOptions<BsonDocument, BsonDocument>();
            Task<IAsyncCursor<BsonDocument>> x = Task.FromResult(await col.MapReduceAsync<BsonDocument>(mapJS, reducerJS));
           

            return x.Result;
        }
    }
}

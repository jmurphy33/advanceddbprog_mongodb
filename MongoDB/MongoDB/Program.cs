﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace MongoDB
{
    class Program
    {
        static string col = "", field = "", value = "", newField = "", newValue = "";
        static Database db;
        static void Main(string[] args)
        {
            try
            {

                setupClasses();

                Console.WriteLine("1: \t READ DOC" +
                                "\n2: \t READ ALL DOCS" +
                                "\n3: \t READ CERTAIN DOCS" +
                                "\n4: \t CREATE DOC" +
                                "\n5: \t CREATE MANY DOCS" +
                                "\n6: \t DELETE ALL DOCS" +
                                "\n7: \t DELETE DOC " +
                                "\n8: \t DELETE CERTAIN DOCS" +
                                "\n9: \t UPDATE DOC" +
                                "\n10: \t UPDATE ALL DOCS" +
                                "\n11: \t UPDATE CERTAIN DOCS" +
                                "\n12: \t MAP REDUCE" +
                                "\n------------------------------------------------"
                                );

                string number = Console.ReadLine();
                // read
                if (number == "1") { Read_Document(); }
                else if (number == "2") { Read_AllDocumets(); }
                else if (number == "3") { Read_CertainDocuments(); }
                // create
                else if (number == "4") { Create_Document(); }
                else if (number == "5") { Create_ManyDocuments(); }
                // delete
                else if (number == "6") { Delete_AllDocuments(); }
                else if (number == "7") { Delete_Document(); }
                else if (number == "8") { Delete_CertainDocuments(); }
                // update
                else if (number == "9") { Update_Document(); }
                else if (number == "10") { Update_AllDocuments(); }
                else if (number == "11") { Update_ManyDocuments(); }
                // map reduce
                else if (number == "12") { MapReduce(); }
                // error
                else { Console.WriteLine("Not an option...ending program"); }

                Console.WriteLine("Worked...");
                Console.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.Read();
            }
        }


        public static void setupClasses()
        {
            db = new Database();
        }


        static void Create_Document()
        {
            // write new collection
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("New Doc value: ");
            newValue = Console.ReadLine();
            db.Create_Document(col, newValue);

        }

        static void Create_ManyDocuments()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("New Doc value: ");
            newField = Console.ReadLine();
            BsonDocument doc;
            int i = 0;
            var list = new List<BsonDocument>();

            while (i < 10)
            {
                doc = BsonDocument.Parse(newField);
                list.Add(doc);
                i++;
            }


            db.Create_ManyDocuments(col, list);
        }

        // R - Reads

        static async void Read_Document()
        {
            // read a single document
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("Field: ");
            field = Console.ReadLine();
            Console.WriteLine("Value: ");
            value = Console.ReadLine();
            var x = db.Read_Document(col, field, value).Result as IAsyncCursor<BsonDocument>;

            while (await x.MoveNextAsync())
            {
                var item = x.Current;
                foreach (var document in item)
                {
                    Console.WriteLine(document.ToJson());
                }
            }
        }

        static async void Read_CertainDocuments()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("Field: ");
            field = Console.ReadLine();
            Console.WriteLine("Value: ");
            value = Console.ReadLine();
           var x = db.Read_ManyDocuments(col, field, value).Result as IAsyncCursor<BsonDocument>;

            while (await x.MoveNextAsync())
            {
                var item = x.Current;
                foreach (var document in item)
                {
                    Console.WriteLine(document.ToJson());
                }
            }
        }

        static async void Read_AllDocumets()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
           var x = db.Read_AllDocuments(col).Result as IAsyncCursor<BsonDocument>;

            while (await x.MoveNextAsync())
            {
                var item = x.Current;
                foreach (var document in item)
                {
                    Console.WriteLine(document.ToJson());
                }
            }
        }


        // U - Updates

        static void Update_AllDocuments()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("Enter field: ");;
            newField = Console.ReadLine();
            Console.WriteLine("Enter value: "); ;
            newValue = Console.ReadLine();
            db.Update_AllDocuments(col, newField, newValue);
        }

        static void Update_ManyDocuments()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("Search Field: ");
            field = Console.ReadLine();
            Console.WriteLine("Search Value: ");
            value = Console.ReadLine();
            Console.WriteLine("Field: ");
            newField = Console.ReadLine();
            Console.WriteLine("Value: ");
            newValue = Console.ReadLine();
            db.Update_ManyDocuments(col, field, value, newField, newValue);
        }

        static void Update_Document()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("Search Field: ");
            field = Console.ReadLine();
            Console.WriteLine("Search Value: ");
            value = Console.ReadLine();
            Console.WriteLine("Update Field: ");
            newField = Console.ReadLine();
            Console.WriteLine("Update Value: ");
            newValue = Console.ReadLine();

            db.Update_Document(col, field, value, newField, newValue);

        }

        // D - Deletes
        static void Delete_AllDocuments()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            db.Delete_AllDocuments(col);
        }

        static void Delete_CertainDocuments()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("Field: ");
            field = Console.ReadLine();
            Console.WriteLine("Value: ");
            value = Console.ReadLine();
            
            db.Delete_ManyDocuments(col, field, value);
        }

        static void Delete_Document()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            Console.WriteLine("New Doc field: ");
            newField = Console.ReadLine();
            db.Delete_Document(col, newField);
        }

        // Map reduce 

        static async void MapReduce()
        {
            Console.WriteLine("Collection: ");
            col = Console.ReadLine();
            var x = db.MapReduce(col).Result as IAsyncCursor<BsonDocument>;

            while (await x.MoveNextAsync())
            {
                Console.WriteLine(x.Current.ToJson());

            }

        }
    }
}
